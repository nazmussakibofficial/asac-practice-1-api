'use strict';

const Hapi = require('@hapi/hapi');
const sharp = require('sharp');
const tou8 = require('buffer-to-uint8array');


const init = async () => {

    const server = Hapi.server({
        port: 5002,
        host: 'localhost',
        routes: {
            cors: {
                origin: ["*"],
            },
        },
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {

            return 'Hello World!';
        }
    });

    server.route({
        method: 'POST',
        path: '/type-check',
        options: {
            payload: {
                parse: true,
                output: "data",
                allow: 'multipart/form-data',
                multipart: {
                    output: "data"
                },
            }
          },
        handler: (request, h) => {
            const data = request.payload.image;
            const arr = tou8(data).subarray(0, 4);
            let header = "";
            let isValidHeader;
            for(let i = 0; i < arr.length; i++) {
                header += arr[i].toString(16);
            }
            if(header === ('ffd8ffe0' || 'ffd8ffe1' || 'ffd8ffe2' || 'ffd8ffe3' || 'ffd8ffe8' || 'ffd8ffdb')){
                isValidHeader = true;
            }
            else{
                isValidHeader = false
                // *Sharp Conversion*
                sharp(data)
                .resize({
                    width: 300,
                    height: 350,
                    fit: 'fill',
                  })
                .toFile('output.jpg', (err, info) => {  });
            }
            
            return {isValidHeader, header};
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();